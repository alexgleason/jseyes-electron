jseyes-electron
===============

xeyes clone in Electron based on [felixmc/jQuery-xeyes](https://github.com/felixmc/jQuery-xeyes).

* To install, ensure `electron-prebuilt` is installed, cd into the project directory, and run `npm install`.
* To start, run `electron .`. Linux users must run `electron . --enable-transparent-visuals --disable-gpu` instead to enable transparency.

![screenshot from 2015-10-08 02 01 41](https://cloud.githubusercontent.com/assets/3639540/10359772/3a9129e6-6d61-11e5-967d-141845c125b3.png)
